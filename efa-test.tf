provider "aws" {
  region = "eu-west-3"
}

resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "public" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.0.0/24"
  availability_zone       = "eu-west-3a"
  map_public_ip_on_launch = true
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "10.0.0.0/16"
    gateway_id = "local"
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}

resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.public.id
}

resource "aws_security_group" "efa-sg" {
  name        = "efa-sg"
  description = "Security group for the private subnet"
}

# Allow all traffic from within security group EFA
resource "aws_vpc_security_group_ingress_rule" "efa-sg" {
  security_group_id            = aws_security_group.efa-sg.id
  referenced_security_group_id = aws_security_group.efa-sg.id
  ip_protocol                  = -1
}

# Allow ingress SSH from anywhere
resource "aws_vpc_security_group_ingress_rule" "ssh" {
  security_group_id = aws_security_group.efa-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  from_port         = 22
  to_port           = 22
  ip_protocol       = "tcp"
}

# Allow all outbound traffic
resource "aws_vpc_security_group_egress_rule" "all" {
  security_group_id = aws_security_group.efa-sg.id
  cidr_ipv4         = "0.0.0.0/0"
  ip_protocol       = -1
}

data "cloudinit_config" "instance_provision" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "install_eessi.sh"
    content_type = "text/x-shellscript"
    content      = file("install_eessi.sh")
  }

  part {
    filename     = "host_injection.sh"
    content_type = "text/x-shellscript"
    content      = file("host_injection.sh")
  }
}

resource "aws_network_interface" "efa" {
  subnet_id       = aws_subnet.public.id
  private_ips     = ["10.0.0.50"]
  security_groups = [aws_security_group.efa-sg.id]
}

resource "aws_network_interface_attachment" "attach-efa" {
  instance_id          = aws_instance.compute.id
  network_interface_id = aws_network_interface.efa.id
  device_index         = 0
}

resource "aws_instance" "compute" {
  subnet_id       = aws_subnet.public.id
  instance_type   = "t2.small"
  ami             = "ami-07b9b3580fd50d52d" # Rocky 8
  user_data       = data.cloudinit_config.instance_provision.rendered
  key_name        = "dev"
  security_groups = [aws_security_group.efa-sg.id]
}
